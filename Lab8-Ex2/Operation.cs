﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8_Ex2
{
    class Operation
    {
        public int Id;
        public DateTime Time;
        public int Sum;
        public static int StartSum;
        public static int FinishSum;
        public static DateTime MaxTime;
        public string Action { get; set; }
    }
}
